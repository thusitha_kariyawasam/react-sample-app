/* flow */
import {combineReducers, createStore, applyMiddleware, compose} from 'redux';
import createSagaMiddleware from 'redux-saga';
import logger from 'redux-logger';
import rootReducer from '../reducers';
import { routerMiddleware } from 'react-router-redux';
import { routerChange } from '../middleware/router.change.middleware';

const sagaMiddleware = createSagaMiddleware();

export default function configureStore (history) {
    const store = createStore(
        combineReducers({
            rootReducer
        }),
        applyMiddleware(
            sagaMiddleware,
            logger(),
            routerMiddleware(history),
            routerChange(history)
        )
    );

    if (module.hot) {
        module.hot.accept('../reducers/index', () => {
            const nextRootReducer = require('../reducers/index').default;
            store.replaceReducer(nextRootReducer);
        });
    }

    return store;
}
