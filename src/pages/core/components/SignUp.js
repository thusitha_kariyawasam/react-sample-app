import React from 'react';
import { Button } from 'react-bootstrap';
import { SignUpMoal } from '../../core/index';

type Props = {
  isOpen: boolean,
  hideModal: boolean,
  updateMenu: (isOpenMenu: boolean) => void,
  updateValidation: () => void,
  signUpValidation: () => void
};

/**
 * The SignUp for the website.
 */
export function SignUp({ isOpen, hideModal, signUpValidation, updateValidation, updateMenu } : Props): React.Element<*> {
    return (
        <div className="sign-up-container">
            <Button bsStyle="default" bsSize="small" onClick={() => {
                signUpValidation = {
                    status: null,
                    value: ''
                };
                if (updateMenu) {
                    updateMenu();
                }
                updateValidation(() => signUpValidation);
                hideModal(() => isOpen = true);
            }
            }>SIGN UP</Button>
            <SignUpMoal signUpValidation={signUpValidation} updateValidation={updateValidation} isOpen={isOpen} hideModal={hideModal}/>
        </div>
    );
}

export default SignUp;
