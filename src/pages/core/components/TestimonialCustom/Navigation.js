import React from 'react';
/**
 * Testimonial Navigation Component.
 */

const PagePosition = ({currentPage, index, updatePage, animation, updateAnimation}) => {
  return (
    <li className={index===currentPage? 'active':''} onClick={() => {
      updateAnimation(()=> animation = '');
      setTimeout(()=> {
        updatePage(()=> currentPage=index);
        updateAnimation(()=> animation = 'fadeIn 2s');
      },1000);
    }}/>
  );
};

export function Navigation({content, currentPage, updatePage, animation, updateAnimation}): React.Element<*>{
  return (
    <div className="navigation">
      <ul>
        {content && content.map((content, index) =>
          <PagePosition 
            key={index}
            index={index} 
            currentPage={currentPage} 
            updatePage={updatePage}
            animation={animation} 
            updateAnimation={updateAnimation}/>
        )}
      </ul>
    </div>
  );
}

export default Navigation;