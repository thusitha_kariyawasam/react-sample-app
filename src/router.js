import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import { AsyncPageWrapper, AsyncMainPageWrapper } from './pages';
import createHistory from 'history/createBrowserHistory';

let history = createHistory();

export default (
    <AsyncPageWrapper displayName={1337} history={history}>
        <Switch>
            <Route exact={true} path="/" component={AsyncMainPageWrapper}/>
            <Redirect to="/"/>
        </Switch>
    </AsyncPageWrapper>
);
