'use strict';

export const CHANGE_ROUTE = 'CHANGE_ROUTE';

export function changeRoute(payload) {
    return {type: CHANGE_ROUTE, payload: payload};
}
