import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom';
import router from './router';

import './styles/main.scss';
import '../node_modules/aos/src/sass/aos.scss';

ReactDOM.render(
    <Router>{router}</Router>,
    window.document.getElementById('app')
);

if (process.env.NODE_ENV !== 'production') {
    if (module.hot) {
        module.hot.accept();
    }
}
