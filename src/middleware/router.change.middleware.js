'use strict';

import {CHANGE_ROUTE} from '../actions/route.actions';

export const routerChange = history => store => next => action => {
    if (action.type === CHANGE_ROUTE) {
        history.push(action.payload);
    } else {
        next(action);
    }
};
