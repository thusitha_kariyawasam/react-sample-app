/* @flow */
import {compose, withState, lifecycle} from 'recompose';
import Scroll from 'react-scroll';

let Events = Scroll.Events, scrollSpy = Scroll.scrollSpy, scroll = Scroll.animateScroll;

/**
 * Connect necessary data to the outer whole Page component.
 */
export default compose(
    withState('isOpen', 'hideModal', false),
    withState('signUpValidation', 'updateValidation', {status: null, value: ''}),
    withState('isOpenMenu', 'updateMenu', false),
    lifecycle({
        componentWillMount() {
            scroll.scrollToTop();

            Events.scrollEvent.register('begin', function () {
            });

            Events.scrollEvent.register('end', function () {
            });

            scrollSpy.update();
        },
        componentWillUnmount() {
            Events.scrollEvent.remove('begin');
            Events.scrollEvent.remove('end');
        }
    })
);
