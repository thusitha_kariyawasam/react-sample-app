// flow-typed signature: 7fcce89a4c29e7baa0474858bdaf166e
// flow-typed version: <<STUB>>/babel-plugin-try-catch-wrapper_v^0.2.2/flow_v0.60.1

/**
 * This is an autogenerated libdef stub for:
 *
 *   'babel-plugin-try-catch-wrapper'
 *
 * Fill this stub out by replacing all the `any` types.
 *
 * Once filled out, we encourage you to share your work with the
 * community by sending a pull request to:
 * https://github.com/flowtype/flow-typed
 */

declare module 'babel-plugin-try-catch-wrapper' {
  declare module.exports: any;
}

/**
 * We include stubs for each file inside this npm package in case you need to
 * require those files directly. Feel free to delete any files that aren't
 * needed.
 */
declare module 'babel-plugin-try-catch-wrapper/lib/babel' {
  declare module.exports: any;
}

declare module 'babel-plugin-try-catch-wrapper/lib/index' {
  declare module.exports: any;
}

// Filename aliases
declare module 'babel-plugin-try-catch-wrapper/lib/babel.js' {
  declare module.exports: $Exports<'babel-plugin-try-catch-wrapper/lib/babel'>;
}
declare module 'babel-plugin-try-catch-wrapper/lib/index.js' {
  declare module.exports: $Exports<'babel-plugin-try-catch-wrapper/lib/index'>;
}
