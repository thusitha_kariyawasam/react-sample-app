/* @flow */
import React from 'react';

import { Header, greenLogo } from '../core/index';
import enhancePageWrapper from '../containers/enhancePageWrapper';

type Props = {
  children?: any,
};
// noinspection JSAnnotator
/**
 * Root Component for the site.
 */
export function PageWrapper({children, ...otherProps}: Props): React.Element<*> {
    return (
        <div id="main-wrapper">
            <Header id="site-header" logoImgSrc={greenLogo} {...otherProps}/>
            {children}
        </div>
    );
}

export const EnhancedPageWrapper = enhancePageWrapper(PageWrapper);
export default EnhancedPageWrapper;
