import React from 'react';
import { asyncComponent } from 'react-async-component';

export const AsyncPageWrapper = asyncComponent({
    resolve: () => System.import('./PageWrapper'),
    LoadingComponent: () => <div className="loading"><img src="/assets/spinner.gif"/></div>
});

export const AsyncMainPageWrapper = asyncComponent({
    resolve: () => System.import('./MainPageWrapper'),
    LoadingComponent: () => <div className="loading"><img src="/assets/spinner.gif"/></div>
});
