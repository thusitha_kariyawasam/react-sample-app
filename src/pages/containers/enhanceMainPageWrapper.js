import { compose, withState } from 'recompose';

/**
 * Connect necessary data to the outer main Page component.
 */
const value = {status: null, value: ''};
const contactUs = {'firstName': value, 'lastName': value, 'email': value, 'company': value, 'regarding': value};
export default compose(
    withState('currentPage', 'updatePage', 0),
    withState('animation', 'updateAnimation', ''),
    withState('contactUsValidation', 'updateContactUs', contactUs),
    withState('alert', 'updateAlert', {status: '', text: ''})
);
