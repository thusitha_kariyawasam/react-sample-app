/* @flow */
import React from 'react';
import {ToastContainer, toast} from 'react-toastify';
import {Col} from 'react-bootstrap';

type Props = {
    alert: { status: any, text: any }
};

/**
 * Main Slide Component.
 */
export function MainSlide({alert}: Props): React.Element<*> {
    const alertMaintain = () => {
        switch (alert.status) {
            case 'success' :
                toast.success(alert.text, {
                    position: toast.POSITION.TOP_RIGHT
                });
                break;
            case 'error' :
                toast.error(alert.text, {
                    position: toast.POSITION.TOP_RIGHT
                });
                break;
        }
    };
    return (
        <div className="main-slide-container">
            <div className="main-slide-alert">
                <ToastContainer autoClose={5000} className="alert-container">
                    {alertMaintain()}
                </ToastContainer>
            </div>
            <div className="slide-detail">
                <Col xs={12} sm={6} md={6} className="facility container">
                    <h2>sample sample sample</h2>
                    <p>
                        sample, sample, sample
                    </p>
                </Col>
            </div>
        </div>
    );
}

export default MainSlide;
