import React from 'react';
import {Navigation, TextContainer} from '../../core/index';
/**
 * Testimonial Component.
 */

/*
 * Need to configure data for testimonial
 *
 **/
const content = [
  {image: 'avatar_1.png',name: 'dummy 5', position: 'CEO, IBM', text: 'First step was a phone'},
  {image: 'avatar_2.png',name: 'dummy 1', position: 'CEO, IBM', text: 'Dummy 1 First step was a phone '},
  {image: 'avatar_3.png',name: 'Dummy 2', position: 'CEO, IBM', text: 'Dummy 2 First step was a phone '},
  {image: 'avatar_4.png',name: 'Dummy 3', position: 'CEO, IBM', text: 'Dummy 3 First step was a phone '},
];
export function Testimonial({...otherProps }): React.Element<*>{
  return (
    <div className="testimonial-container">
      <div className="container">
        <h2>What People Say</h2>
        <TextContainer content={content} {...otherProps}/>
        <Navigation content={content} {...otherProps}/>
      </div>
    </div>
  );
}

export default Testimonial;
