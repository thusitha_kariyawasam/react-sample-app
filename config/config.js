
const Email = {
    signUp: process.env.NODE_ENV !== 'production' ? 'test@gmail.com' : '',
    contact: process.env.NODE_ENV !== 'production' ? 'test@gmail.com' : '',
    list: '',
    origin: process.env.NODE_ENV !== 'production' ? 'http://localhost:3001' : ''
};

export default Email;
