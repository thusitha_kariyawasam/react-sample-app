import React from 'react';
import { Image } from 'react-bootstrap';
/**
 * Testimonial Text Container Component.
 */
export function TextContainer({animation, content, currentPage, updatePage, updateAnimation}): React.Element<*>{
  let style = {
    animation : animation
  };
  return (
    <div className={`text-container`}>
          <div className="quotation"/>
          <div className="top-navi">
            <div className="left" onClick={() => {
              if (currentPage > 0) {
                currentPage--;
              } else {
                currentPage = content.length-1;
              }
              updateAnimation(()=> animation = '');
              setTimeout(()=> {
                updatePage(()=> currentPage);
                updateAnimation(()=> animation = 'rightToLeft 1s');
              },1000);
            }}/>
            <p style={style}>{content[currentPage].text}</p>
            <div className="right" onClick={() => {
              if (currentPage < content.length-1) {
                currentPage++;
              } else {
                currentPage = 0;
              }
              updateAnimation(()=> animation = '');
              setTimeout(()=> {
                updatePage(()=> currentPage);
                updateAnimation(()=> animation = 'leftToRight 1s');
              },1000);
            }}/>
          </div>
          <div style={style}>
            <Image src={`/assets/testimonial/${content[currentPage].image}`}/>
            <h3 style={{fontWeight: 'bold'}}>{content[currentPage].name}</h3>
            <h4>{content[currentPage].position}</h4>
          </div>
    </div>
  );
}

export default TextContainer;
