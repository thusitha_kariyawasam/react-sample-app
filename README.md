# Sample app

## Development Scripts

### Install dependencies
To install all the needed dependencies, use:
```
npm install
```

### Start server
To start the static server, use:
```
npm start
```

*Go to http://localhost:3000 to view sample page.*

To start in development (watch) mode, use:
```
npm start
```
*Go to http://localhost:3000 to view sample page.*

### Tests
To perform all the tests (using flowtype and jest), use:
```
npm test
```