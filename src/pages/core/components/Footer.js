import React from 'react';
import { Col } from 'react-bootstrap';
import Scroll from 'react-scroll';
let Link = Scroll.Link;

/**
 * The footer for the website.
 */
export function Footer(){
    return (
        <footer className="footer-container">
            <div className="container">
                <Col xs={12} sm={4} md={4} className="pages">
                    <p>
                        <Link to="contact-us-container" spy={true} smooth={true} offset={50} duration={1000}>
                            Contact us
                        </Link>
                    </p>
                </Col>
                <Col xs={12} sm={4} md={4} className="version"/>
            </div>
        </footer>
    );
}

export default Footer;
