/* @flow */
import React from 'react';
import { Button, Col, ControlLabel, FormGroup, FormControl, Form, HelpBlock, Popover, OverlayTrigger } from 'react-bootstrap';
import ReCAPTCHA from 'react-google-recaptcha';
import siteKey from '../../../../config/siteKey';
import { post } from '../../../helpers/request.helper';

type valueType = {status?: any, value: string};
type contactUs = {'firstName': valueType, 'lastName': valueType, 'email': valueType, 'company': valueType, 'regarding': valueType};

type Props = {
    contactUsValidation: contactUs,
    updateContactUs: (contactUs) => void,
    updateAlert: ({
        status: string,
        text: string
    }) => void
};
/**
 * ContactUs Component.
 */
const reg = /\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/;
const value = {status: null, value: ''};
const initialContactUs = {'firstName': value, 'lastName': value, 'email': value, 'company': value, 'regarding': value};
let subscribe;

export function ContactUs({contactUsValidation, updateContactUs, updateAlert, ...otherProps}: Props): React.Element<*> {

    const validation = (type, value) => {
        if (type === 'email' && reg.test(value)) {
            contactUsValidation[type] = {
                status: 'success',
                value: value
            };
        } else if (type !== 'email' && value) {
            contactUsValidation[type] = {
                status: 'success',
                value: value
            };
        } else {
            contactUsValidation[type] = {
                status: 'error',
                value: value
            };
        }
        updateContactUs(contactUsValidation);
    };

    const captchaChange = (value) => {
        if (value) {
            sendEmail();
        }
    };

    const sendEmail = () => {
        const body = {
            email: contactUsValidation['email'].value,
            firstName: contactUsValidation['firstName'].value,
            lastName: contactUsValidation['lastName'].value,
            company: contactUsValidation['company'].value,
            regarding: contactUsValidation['regarding'].value,
            subscribe: subscribe
        };

        post('/contact-us', body)
            .then(() => {
                const alert = {
                    status: 'success',
                    text: 'Email has been sent successfully'
                };
                updateAlert(alert);
                setTimeout(() => {
                    const alert = {
                        status: '',
                        text: ''
                    };
                    updateAlert(alert);
                    updateContactUs(initialContactUs);
                }, 3000);
            })
            .catch(() => {
                const alert = {
                    status: 'error',
                    text: 'Some thing went wrong !'
                };
                updateAlert(alert);
            });
    };

    const manageCaptcha = (
        <Popover id="popover-manage" className="contact-popover captcha">
            <ReCAPTCHA
                sitekey={siteKey.key}
                onChange={captchaChange}
            />
        </Popover>
    );

    const handleSubscribe = (evt) => {
        subscribe = evt.currentTarget.checked;
    };

    const allAreDone = contactUsValidation['firstName'].status === 'success'
                          && contactUsValidation['lastName'].status === 'success'
                          && contactUsValidation['email'].status === 'success'
                          && contactUsValidation['company'].status === 'success'
                          && contactUsValidation['regarding'].status === 'success';

    return (
        <div id="contact-us-container" {...otherProps}>
            <div className="container">
                <h2>Contact Us</h2>
                <div className="contact-us-form">
                    <Form>
                        <div className="row">
                            <Col xs={12} sm={6} md={6}>
                                <FormGroup controlId="firstName" validationState={contactUsValidation['firstName'].status}>
                                    <ControlLabel>First Name<span>*</span></ControlLabel>
                                    <FormControl type="text" placeholder="Enter first name" onChange={(evt) => {
                                        const firstName = evt.currentTarget.value;
                                        validation('firstName', firstName);
                                    }}/>
                                    {contactUsValidation['firstName'].status === 'error' && <HelpBlock>First Name is required.</HelpBlock>}
                                </FormGroup>
                            </Col>
                            <Col xs={12} sm={6} md={6}>
                                <FormGroup controlId="lastName" validationState={contactUsValidation['lastName'].status}>
                                    <ControlLabel>Last Name<span>*</span></ControlLabel>
                                    <FormControl type="text" placeholder="Enter last name" onChange={(evt) => {
                                        const lastName = evt.currentTarget.value;
                                        validation('lastName', lastName);
                                    }}/>
                                    {contactUsValidation['lastName'].status === 'error' && <HelpBlock>Last Name is required.</HelpBlock>}
                                </FormGroup>
                            </Col>
                            <Col xs={12} sm={6} md={6}>
                                <FormGroup controlId="emailAddress" validationState={contactUsValidation['email'].status}>
                                    <ControlLabel>Email Address<span>*</span></ControlLabel>
                                    <FormControl type="email" placeholder="Your email address" onChange={(evt) => {
                                        const email = evt.currentTarget.value;
                                        validation('email', email);
                                    }}/>
                                    {contactUsValidation['email'].status === 'error' && contactUsValidation['email'].value && <HelpBlock>Email is invalid.</HelpBlock>}
                                    {contactUsValidation['email'].status === 'error' && !contactUsValidation['email'].value && <HelpBlock>Email is required.</HelpBlock>}
                                </FormGroup>
                            </Col>
                            <Col xs={12} sm={6} md={6}>
                                <FormGroup controlId="organizationName" validationState={contactUsValidation['company'].status}>
                                    <ControlLabel>Organization Name<span>*</span></ControlLabel>
                                    <FormControl placeholder="Organization name" type="text" onChange={(evt) => {
                                        const company = evt.currentTarget.value;
                                        validation('company', company);
                                    }}/>
                                    {contactUsValidation['company'].status === 'error' && <HelpBlock>Organization Name is required.</HelpBlock>}
                                </FormGroup>
                            </Col>
                            <Col xs={12} sm={12} md={12}>
                                <FormGroup controlId="Message" validationState={contactUsValidation['regarding'].status}>
                                    <ControlLabel>Regarding<span>*</span></ControlLabel>
                                    <FormControl componentClass="textarea" placeholder="Regarding..." onChange={(evt) => {
                                        const regarding = evt.currentTarget.value;
                                        validation('regarding', regarding);
                                    }}/>
                                    {contactUsValidation['regarding'].status === 'error' && <HelpBlock>Message field is required.</HelpBlock>}
                                </FormGroup>
                            </Col>
                            <Col xs={12} sm={6} md={6} className="subscribe">
                                <div className="checkbox">
                                    <input type="checkbox" id="subscribe" onChange={handleSubscribe}/>
                                    <label htmlFor="subscribe">Subscribe me to sample updates</label>
                                </div>
                            </Col>
                            <Col xs={12} sm={6} md={6}>
                                <OverlayTrigger trigger={allAreDone ? ['click'] : '[]'} rootClose placement={window.innerWidth < 768 ? 'top' : 'left'} overlay={manageCaptcha}>
                                    <Button id='submitButton' onClick={(evt) => {
                                        evt.preventDefault();
                                        if (!allAreDone) {
                                            for (let key in contactUsValidation) {
                                                validation(key, contactUsValidation[key].value);
                                            }
                                        }
                                    }}>
                                        SUBMIT
                                    </Button>
                                </OverlayTrigger>
                            </Col>
                        </div>
                    </Form>
                </div>

            </div>
        </div>
    );
}

export default ContactUs;
