'use strict';
import {Observable} from 'rxjs';
import Email from '../../config/config';

const ajax = Observable.ajax;

const HEADERS = {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
};

export function post(url, body, headers) {
    headers = Object.assign({}, HEADERS, headers);
    return new Promise((resolve, reject) => {
        ajax.post(Email.origin + url, body, headers).subscribe(response => {
            return resolve(response.response);
        }, err => {
            return reject({
                status: err.xhr.status,
                response: err.xhr.response
            });
        });
    });
}
