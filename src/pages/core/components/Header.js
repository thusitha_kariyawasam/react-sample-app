/* @flow */
import React from 'react';
import { Col } from 'react-bootstrap';
import { SignUp } from '../../core/index';
import Scroll from 'react-scroll';
let Link = Scroll.Link;

type Props = {
  children: any,
  history: {location: {pathName: string}},
  isOpenMenu: boolean,
  updateMenu: (isOpenMenu: boolean) => void,
  updateValidation: () => void,
  signUpValidation: () => void
};

/**
 * The header for the website.
 */
export function Header({children, signUpValidation, updateMenu, updateValidation, history, isOpenMenu, ...otherProps }: Props): React.Element<*> {
    const {location} = history;
    return (
        <header className="header-container">
            <div className="container">
                <Col xs={12} sm={12} md={12} className="right hidden-xs">
                    {location.pathname === '/' && <Link to="contact-us-container" spy={true} smooth={true} offset={50} duration={1000}>
                       CONTACT
                    </Link>}
                    {location.pathname !== '/' && <a href="/">CONTACT</a>}
                    <SignUp signUpValidation={signUpValidation} updateValidation={updateValidation} {...otherProps}/>
                </Col>
                <Col xs={4} className="right hidden-lg hidden-md hidden-sm">
                    <a className="menu_button" onClick={() => { isOpenMenu = !isOpenMenu; updateMenu(isOpenMenu); }}/>
                </Col>
                <div className={ !isOpenMenu ? 'mobile_menu' : 'mobile_menu open hidden-lg hidden-md hidden-sm'}>
                    <div className="close-menu" onClick={() => { isOpenMenu = false; updateMenu(isOpenMenu); }}/>
                    <Col xs={12} sm={12} md={12} className="link">
                        {location.pathname === '/' && <Link to="contact-us-container" spy={true} smooth={true} offset={50} duration={1000} onClick={() => { isOpenMenu = false; updateMenu(isOpenMenu); }}>
                          CONTACT
                        </Link>}
                        {location.pathname !== '/' && <a href="/" onClick={() => { isOpenMenu = false; updateMenu(isOpenMenu); }}>CONTACT</a>}
                    </Col>
                    <Col xs={12} sm={12} md={12}>
                        <SignUp
                            signUpValidation={signUpValidation}
                            updateValidation={updateValidation}
                            updateMenu={() => { isOpenMenu = false; updateMenu(isOpenMenu); }}
                            {...otherProps}/>
                    </Col>
                </div>
            </div>
            {children}
        </header>
    );
}

export default Header;
