import React from 'react';
import { Button, Modal, FormGroup, FormControl, Form, HelpBlock, CloseButton, Popover, OverlayTrigger } from 'react-bootstrap';
import siteKey from '../../../../config/siteKey';
import ReCAPTCHA from 'react-google-recaptcha';

/**
 * The SignUp Modal for the website.
 */
const reg = /\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/;

export function SignUpModal({ isOpen, hideModal, signUpValidation, updateValidation, ...otherProps }: Props): React.Element<*> {
    let hasValue;
    let style = {
        animation: 'fadeIn 1s'
    };
    const captchaChange = (value) => {
        if (value) {
            // TODO need cal API
            // captcha.reset();
            hasValue = true;
        }
    };
    const manage = (
        <Popover id="popover-manage" className="signup-popover">
            <ReCAPTCHA
                sitekey={siteKey.key}
                onChange={captchaChange}
            />
        </Popover>
    );
    return (
        <Modal
            show={isOpen}
            onHide={hideModal}
            dialogClassName="sign-up-modal"
            {...otherProps}
            animation={false} style={isOpen ? style : ''}>
            <Modal.Header>
                <CloseButton onClick={() => hideModal(() => isOpen = false)}/>
                <Modal.Title>Sign Up</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <h4>Sign up sample</h4>
                <div className="sign-up-form">
                    <Form inline>
                        <FormGroup controlId="formInlineName" validationState={signUpValidation.status}>
                            <FormControl type="email"
                                onChange={(evt) => {
                                    const email = evt.currentTarget.value;
                                    if (reg.test(email)) {
                                        signUpValidation = {
                                            status: 'success',
                                            value: email
                                        };
                                    } else {
                                        signUpValidation = {
                                            status: 'error',
                                            value: email
                                        };
                                    }
                                    updateValidation(() => signUpValidation);
                                }}
                                placeholder="Email Address" required/>
                            {signUpValidation.status === 'error' && signUpValidation.value && <HelpBlock>Email is invalid.</HelpBlock>}
                            {signUpValidation.status === 'error' && !signUpValidation.value && <HelpBlock>Email is required.</HelpBlock>}
                        </FormGroup>
                        <OverlayTrigger trigger={(signUpValidation.status === 'success' && !hasValue) ? ['click'] : 'none'} rootClose placement="top" overlay={manage}>
                            <Button id='manageButton' onClick={() => {
                                if (signUpValidation.value && signUpValidation.status === 'success') {
                                    if (hasValue) {
                                        sendMail();
                                    }
                                } else {
                                    signUpValidation = {
                                        status: 'error',
                                        value: ''
                                    };
                                    updateValidation(() => signUpValidation);
                                }
                            }}>
                            SUBMIT
                            </Button>
                        </OverlayTrigger>
                        <FormGroup>
                            <div className="checkbox-sign-up">
                                <input type="checkbox" id="subscribe-signup"/>
                                <label htmlFor="subscribe-signup">Subscribe me to sample updates</label>
                            </div>
                        </FormGroup>
                    </Form>
                </div>
            </Modal.Body>
        </Modal>
    );
}

export default SignUpModal;
