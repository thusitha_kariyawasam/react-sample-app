/* @flow */
import React from 'react';

import { ContactUs, Footer, greenLogo, MainSlide, Testimonial } from '../core/index';
import enhanceMainPageWrapper from '../containers/enhanceMainPageWrapper';
type Props = {
  alert: {status?: any, text?: any},
  updateAlert: ()=> void,
  updateContactUs:() => void,
  contactUsValidation: ()=> void
};
// noinspection JSAnnotator
/**
 * Root Component for the site.
 */
export function MainPageWrapper({alert, updateAlert, contactUsValidation, updateContactUs, ...otherProps}: Props): React.Element<*> {
    return (
        <div className="wrapper">
            <MainSlide alert={alert}/>
            <div className="divider"/>
            <Testimonial {...otherProps}/>
            <ContactUs contactUsValidation={contactUsValidation} updateContactUs={updateContactUs} updateAlert={updateAlert} alert={alert}/>
            <Footer logoUrl={greenLogo}/>
        </div>
    );
}

export const EnhancedMainPageWrapper = enhanceMainPageWrapper(MainPageWrapper);
export default EnhancedMainPageWrapper;
